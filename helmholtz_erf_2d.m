function [Kr,Ki,M,omega] = helmholtz_erf_2d(m,f,dom,pml)

if nargin<4, pml=[]; end
[K,M,omega] = helmholtz_2d(m,f,dom,pml);	% complex-valued Helmholtz data
Kr = real(K);								% real part of K
Ki = imag(K);								% imaginary part of K