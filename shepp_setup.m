%% Problem setup and parameters
fmin = 1;							% minimum frequency
fmax = 5;							% maximum frequency
nf = 5;								% number of frequencies
all_freqs = linspace(fmin,fmax,nf);	% frequencies
ns = 10;							% number of sources
nr = 50;							% number of receivers
ctr = .5;							% contrast
sigma = 1e-5;						% noise level
smoothness = .5;					% intensity of smoothing filter
window_info.type = 'disk';
window_info.center = [.5 .5];
window_info.radius = .3;

%% Shepp-Logan phantom
nx = ceil(10*fmax);				% number of gridpoints in a given direction
dom = domain([0 1 0 1],[nx nx]);	% square domain with equispaced gridpoints
c_true = dom.mat2vec(phantom2(dom,.35,ctr,smoothness));
c_0 = dom.mat2vec(ones(nx));

source_info.type = 'ring';		% create a ring of sources
source_info.center = [.5 .5];	% center of ring of sources
source_info.radius = .34;		% radius of ring of sources
sources = sources_and_receivers(ns,source_info);
receiver_info.type = 'ring';	% create a ring of receivers
receiver_info.center = [.5 .5];	% center of ring of receivers
receiver_info.radius = .36;		% radius of ring of receivers
receivers = sources_and_receivers(nr,receiver_info);

[~,W] = dom.window(window_info);
wpml = .1;
pml_info.type = 'pml';
pml_info.width = wpml;
[~,PML] = dom.window(pml_info);

%% Preliminary plots of experimental setup
figure

subplot(2,2,1)
dom.imagesc(c_true);
c_vec = caxis;
hold on
dom.plot(receivers(1,:),receivers(2,:),'rx')
dom.plot(sources(1,:),sources(2,:),'go')
hold off
axis square
title('Problem setup')

subplot(2,2,2)
dom.imagesc(c_0)
axis square
title('Initial background velocity')
caxis(c_vec)

subplot(2,2,3)
imagesc(dom.vec2mat(c_true)+PML)
hold on
dom.plot(receivers(1,:),receivers(2,:),'rx')
dom.plot(sources(1,:),sources(2,:),'go')
hold off
title('With PML')
axis square
caxis([c_vec(1) c_vec(2)])

subplot(2,2,4)
imagesc(dom.vec2mat(c_true)+flipud(W))
hold on
dom.plot(receivers(1,:),receivers(2,:),'rx')
dom.plot(sources(1,:),sources(2,:),'go')
hold off
title('With window')
axis square
caxis([c_vec(1) c_vec(2)+1])

drawnow