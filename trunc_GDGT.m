function [G0,D0,newNorm]=trunc_GDGT(G,D,truncTol,verbosity)
if nargin<3, verbosity=0; end
if verbosity, fprintf('\tRank before truncation: %d\n',size(G,2)), end
[Q,R]=qr(G,0);
RDRT=R*D*R';
newNorm=norm(RDRT,'fro');
[V,D]=eig((RDRT+RDRT')/2);
S=abs(diag(D));
[~,ind]=sort(S,'ascend');
norms=[0;sqrt(cumsum(S(ind).^2))/newNorm];
k=find(norms<truncTol,1,'last');
G0=Q*V(:,ind(k:end));
D0=D(ind(k:end),ind(k:end));
if verbosity, fprintf('\tRank after truncation: %d\n',size(G0,2)), end