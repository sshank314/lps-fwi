%% -- SETUP FILE FOR FIRST-ORDER LIFTING/RELAXATION OF FULL-WAVEFORM INVERSION -- %%
%% Clear and close everything
clear all
close all
clc

maxit = 2500;
ctr = .1;
init = 'smoothed_true';
square_setup

%% Run first-order lifting/relaxation for FWI
fprintf('Spatial dof:%d, inverse prob. dof:%d\n',dom.N,nf*ns*nr)
folr_fwi_window(dom,all_freqs,sources,receivers,c_true,c_0,window_info,sigma,maxit);