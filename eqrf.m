function Y = eqrf(X,type)
if nargin<2, type='vec'; end
switch type
	case 'vec'
		Y = [real(X);imag(X)];
	case 'mat'
		Y = [real(X) -imag(X);imag(X) real(X)];
end