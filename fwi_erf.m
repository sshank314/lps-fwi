function [m,out]=fwi_erf(dom,freqs,sources,receivers,ctrue,c0,sigma,maxit)
%ADJOINT_STATE_2D   Adjoint-state method for full-waveform inversion in 2D.

if isempty(gcp('nocreate')), parpool; end

Nx = dom.N;							% total number of interior grid points
nf = length(freqs);					% number of frequencies
ns = size(sources,2);				% number of sources
nr = size(receivers,2);				% number of receivers
r_ind = dom.loc2ind(receivers);		% flat indices of receivers
b = zeros(2*Nx,ns);					% initialize right-hand sides
d = zeros(2*nr,nf,ns);				% seismic data
I = speye(2*Nx);					% for padding/prolongation operators
S_pad = I(:,[r_ind r_ind+Nx]);		% padding operator
S_sam = S_pad';
mtrue = 1./ctrue.^2;				% true squared slowness
m0 = 1./c0.^2;						% initial squared slowness

% Create discrete deltas at source location
for j = 1:ns
	b(dom.loc2ind(sources(:,j)),j) = 1/(dom.hx*dom.hy);
end

% Generate true data
fprintf('Generating data... frequency: ')
rng(1)	% set seed
parfor i = 1:nf
	fprintf('%d, ',i)
	[Kr,Ki,M,omega]=helmholtz_erf_2d(mtrue,freqs(i),dom);
	A_true = invertA([Kr -Ki;Ki Kr]-omega^2*[M sparse(Nx,Nx);sparse(Nx,Nx) M]);
	for j = 1:ns
		u_true = A_true.apply(b(:,j)); %#ok<*PFBNS>
		d(:,i,j) = S_sam*u_true+sigma*rand(2*nr,1);
	end
end

fprintf('\n')
clear u_true A_true
opt.Niter = maxit;
[m,out] = lbfgs(@adjoint_state_gradient,m0,opt);

	function [J,DJ] = adjoint_state_gradient(m)
		% Compute the gradient of J(m) = .5*|| S*u(m) - d ||^2 via the adjoint state method
		fprintf('Solving Helmholtz, frequency: ')
		DJ = zeros(Nx,1);	% gradient
		J = 0;				% value of objective function
		parfor ii = 1:nf
			fprintf('%d, ',ii)
			[Kr,Ki,M,omega]=helmholtz_erf_2d(m,freqs(ii),dom);
			Ainv = invertA([Kr -Ki;Ki Kr]-omega^2*[M sparse(Nx,Nx);sparse(Nx,Nx) M],1);
			for jj = 1:ns
				u = Ainv.apply(b(:,jj));
				bq = S_pad*(S_sam*u-d(:,ii,jj));
				q = Ainv.applyt(bq);
				DJ = DJ+omega^2*(u(1:Nx).*q(1:Nx)+u(Nx+1:end).*q(Nx+1:end));
				J = J+norm(S_sam*u-d(:,ii,jj))^2;
			end
		end
		fprintf('\n')
	end
end