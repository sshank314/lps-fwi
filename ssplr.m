classdef ssplr
% SSPLR - Symmetric sparse plus low-rank matrix X = S + G*D*G'.
	properties
		S;
		G;
		D;
		n;
	end
	methods
		function X = ssplr(S,G,D)
			X.n = size(S,1);
			if size(G,1) ~= X.n
				error('SSPLR:X and G must have the same number of rows.')
			end
			if size(G,2) ~= size(D,1)
				error('SSPLR:G must have as many columns as D has rows.')
			end
			if size(D,1) ~= size(D,2)
				error('SSPLR:D must be square.')
			end
			if norm(D-D','fro')/norm(D,'fro') > 1e-14
				error('SSPLR:D must be symmetric.')
			end
			X.S = S;
			X.G = G;
			X.D = D;
		end
		
		function Xfull = full(X)
			Xfull = full(X.S) + X.G*X.D*X.G';
		end
		
		function Z = plus(X,Y)
			Z = ssplr(X.S+Y.S,[X.G Y.G],blkdiag(X.D,Y.D));
		end
		
		function Z = minus(X,Y)
			Z = ssplr(X.S-Y.S,[X.G Y.G],blkdiag(X.D,-Y.D));
		end
		
		function Y = uminus(X)
			Y = ssplr(-X.S,X.G,-X.D);
		end
		
		function Y = mtimes(alpha,X)
			Y = ssplr(alpha*X.S,X.G,alpha*X.D);
		end
		
		function alpha = dot(X,Y)
			XGTYG = X.G'*Y.G;
			alpha = trace(X.S*Y.S) + trace((Y.G'*(X.S*Y.G))*Y.D) ...
				+ trace(X.D*(X.G'*(Y.S*X.G))) + trace(X.D*XGTYG*Y.D*XGTYG');
		end
		
		function alpha = norm(X)
			alpha = sqrt(dot(X,X));
		end
		
		function Xsub = subsref(X,s)
			if strcmp(s.type,'.')
				Xsub = builtin('subsref',X,s);
			else
				l_inds = s.subs{1};
				r_inds = s.subs{2};
				Xsub = subsref(X.S,s) + X.G(l_inds,:)*X.D*X.G(r_inds,:)';
			end
		end
		
		function [V,D,flag] = eigs(X,k,sigma,opts)
			if nargin<2, k=6; end
			if nargin<3, sigma='la'; end
			opts.issym = 1;
			Xapp = @(x) X.S*x+X.G*(X.D*(X.G'*x));
			[V,D,flag] = eigs(Xapp,X.n,k,sigma,opts);
		end
		
		function [R,evs] = rank1_proj(X,opts)
			opts.issym = 1;
			Xapp = @(x) X.S*x+X.G*(X.D*(X.G'*x));
			[vec,lambda] = eigs(Xapp,X.n,opts.num,'lm',opts);
			evs = diag(lambda);
			R = ssplr(sparse(X.n,X.n),vec(:,1)*sqrt(lambda(1,1)),1);
		end
	end
end