function folr_fwi_window(dom,all_freqs,sources,receivers,c_true,c_0,window_info,sigma,maxit)
%FOLR_FWI   First-order lifting/relaxation for full-waveform inversion.

% if isempty(gcp('nocreate')), parpool; end

eigs_tol = 1e-8;	% tolerance passed to eigs
num_eigvecs = 2;	% number of eigenvectors to keep 
trunc_tol = 1e-12;	% truncation tolerance
gamma = 1e5;		% parameter for proximal operators (essentially weights the misfit)

[win_inds,~,W] = dom.window(window_info);
nx = dom.N;								% total number of interior grid points
nf = length(all_freqs);						% number of frequencies
ns = size(sources,2);					% number of sources
nr = size(receivers,2);					% number of receivers
nw = length(win_inds);					% number of pixels in window
ndof = 2*nx*nf*ns;						% total number of degrees of freedom
r_ind = dom.loc2ind(receivers);			% flat indices of receivers
b = zeros(2*nx,ns);						% initialize right-hand sides
d = zeros(2*nr,nf,ns);					% seismic data
I = speye(2*nx);						% for padding/prolongation operators
S_pad = I(:,[r_ind r_ind+nx]);			% padding operator
S_sam = S_pad';							% sampling operator
m_true = 1./c_true.^2;					% true squared slowness
m_0 = m_true;								% initial squared slowness
m_0(win_inds) = 1./c_0(win_inds).^2;		% set value outside window to initial value

K = @(Kr,Ki) [Kr -Ki;Ki Kr];
KT = @(Kr,Ki) [Kr' Ki';-Ki' Kr'];
diagUSVT = @(U,S,V) sum(U.*(V*S),2);
all_lambdas = [];

% Create discrete deltas at source location
for j = 1:ns
	b(dom.loc2ind(sources(:,j)),j) = 1/(dom.hx*dom.hy);
end
B = [1;repmat(reshape(b,[],1),nf,1)];

omegas = zeros(nf,1);
Krs = cell(nf,1);
Kis = cell(nf,1);
KKTinvs = cell(nf,1);
for i = 1:nf
	[Krs{i},Kis{i},~,omegas(i)] = helmholtz_erf_2d(m_true,all_freqs(i),dom);
	Krs{i} = Krs{i} - omegas(i)^2*spdiags(m_0,0,nx,nx);
	KKTinvs{i}=invertA(K(Krs{i},Kis{i})*KT(Krs{i},Kis{i})+omegas(i)^4*kron(speye(2),W*W'));
end

% Generate true data
fprintf('Generating data... frequency: ')
rng(1)	% set seed
parfor i = 1:nf
	fprintf('%d, ',i)
	[Kr,Ki,M,omega]=helmholtz_erf_2d(m_true,all_freqs(i),dom);
	A_true = invertA([Kr -Ki;Ki Kr]-omega^2*[M sparse(nx,nx);sparse(nx,nx) M]);
	for j = 1:ns
		u_true = A_true.apply(b(:,j)); %#ok<*PFBNS>
		d(:,i,j) = S_sam*u_true+sigma*rand(2*nr,1);
	end
end

Q_col = [];
for i = 1:nf
	for j = 1:ns
		Q_col = [Q_col;-S_pad*d(:,i,j)];
	end
end
Q = [ 0				sparse(1,nw)	Q_col';
	  sparse(nw,1)	sparse(nw,nw)	sparse(nw,ndof)
	  Q_col			sparse(ndof,nw)	kron(speye(nf*ns),S_pad*S_sam)];
Qgam = gamma*Q;
fprintf('Size of Q: %d\n',size(Q,1))

% % Uncomment to do some sanity checks
% U=rand(10,2);
% S=diag(rand(2,1));
% V=rand(10,2);
% USVT=U*S*V';
% disp('Checking diagUSVT...')
% disp(norm(diagUSVT(U,S,V)-diag(USVT))/norm(diag(USVT)))
% 
% b = ones(1+ndof,1);
% ATb = AT(b);
% US = speye(1+nw+ndof);
% UG = rand(1+nw+ndof,2);
% % UD = [2 0;0 3];
% % UD = eye(2);
% UD = diag(rand(2,1));
% AU = AA(US,UG,UD);
% ip2 = b'*AU;
% ipfro = trace(US*ATb)+trace(UG'*(ATb*UG*UD));
% disp('Checking adjointness of AA and AT...')
% disp(abs(ip2-ipfro)/abs(ip2))
% 
% b = ones(1+ndof,1);
% w = AATinv(b);
% b1 = AA(AT(w),zeros(1+nw+ndof,1),0);
% disp('Checking validity of AATinv...')
% disp(norm(b-b1,'fro')/norm(b,'fro'))
% error('Variable names in sanity check clash with variable names of function!')

% Low-rank plus sparse Douglas Rachford iteration
% Initial iterate X0
% XG = [1;zeros(nw,1)];
% for i = 1:nf
% 	[Kr,Ki,M,omega]=helmholtz_erf_2d(m0,freqs(i),dom);
% 	A_true = invertA([Kr -Ki;Ki Kr]-omega^2*[M sparse(nx,nx);sparse(nx,nx) M]);
% 	for j = 1:ns
% 		XG = [XG; A_true.apply(b(:,j))];
% 	end
% end
XG2 = zeros(2*nx,ns,nf);
parfor i = 1:nf
	[Kr,Ki,M,omega]=helmholtz_erf_2d(m_0,all_freqs(i),dom);
	A_true = invertA([Kr -Ki;Ki Kr]-omega^2*[M sparse(nx,nx);sparse(nx,nx) M]);
	for j = 1:ns
		XG2(:,j,i) = A_true.apply(b(:,j));
	end
end
XG = [1;zeros(nw,1);reshape(XG2,[],1)];
clear XG2
% max(abs(XG(Nx+2:end)-reshape(XG2,[],1)))/max(abs(XG(Nx+2:end)))

% Y0 = X0
YG = XG;
YD = 1;
YS = sparse(1+nw+ndof,1+nw+ndof);

figure
misfit = [];
% Z = sparse(size(Qgam,1),size(Qgam,2));
for i=1:maxit
	fprintf('-- ITERATE %d --\n',i)
	
	% Computer prox separately, store in Z
	fprintf('Prox. for affine subspace indicator/trace... ')
	tic
	[ZS,ZG,ZD]=prox_aff_trace(-YS,[XG YG],blkdiag(2*eye(size(XG,2)),-YD));
	t=toc;
	fprintf('%.2f\n',t)
	
	% Y term, using prof above and X, Y
	YS = YS + ZS;
	YG = [XG YG ZG];
	YD = blkdiag(-eye(size(XG,2)),YD,ZD);
	
	% Projection on PSD cone
	fprintf('Projection onto PSD cone... ')
	tic
	XG = psd_proj(YS,YG,YD);
	t = toc; fprintf('%.2f\n',t)
	
	% Truncating Y
	fprintf('Truncating Y...')
	tic
	[YG,YD] = trunc_GDGT(YG,YD,trunc_tol,1);
	t = toc; fprintf('%.2f\n',t)

% 	% Affine/trace prox, Y_{n+1} = P_traff(X_n)
% 	fprintf('Prox. for affine subspace indicator/trace... ')
% 	tic
% 	[YS,YG,YD]=prox_aff_trace(Z,XG,eye(size(XG,2)));
% 	t=toc;
% 	fprintf('%.2f\n',t)
% 	
% 	% Projection on PSD cone, X_{n+1} = P_psd(Y_{n+1})
% 	fprintf('Projection onto PSD cone... ')
% 	tic
% 	XG = psd_proj(YS,YG,YD);
% 	t = toc; fprintf('%.2f\n',t)
	
	% Output some information
	disp('Eigenvalues when doing PSD projection...')
	disp(diag(lambda));
% 	fprintf('NNZ in sparse matrices: Y=%d, Z=%d\n',nnz(YS),nnz(ZS))
% 	fprintf('Ranks of low-rank factors: X=%d, Y=%d, Z=%d\n',size(XG,2),size(YG,2),size(ZG,2))
	misfit = [misfit abs(trace(XG'*(Q*XG)))];
	subplot(1,3,1)
		dm = XG(2:nw+1)/sign(XG(2));
		m = m_0+W*dm;
		c = 1./sqrt(m);
		dom.imagesc(c)
		title(sprintf('Iterate %d, min=%.10e, max=%.10e',i,min(m),max(m)))
		caxis([min(c) max(c)])
	subplot(1,3,2)
		semilogy(abs(all_lambdas'))
		title('Eigenvalues')
	subplot(1,3,3)
		semilogy(misfit)
		title('Misfit')
	drawnow
end

	function b = AA(S,G,D)
	% Applies A to a matrix of the form X = S + G*D*G', where S is sparse,
	% G has few columns, and D is diagonal. Outputs a vector b = A(X).
% 	b = S(1,1)+G(1,:)*D*G(1,:)';		% initialize b = X_11
% 	ind = nw+2;							% starting index
% 	inds2 = 2:nw+1;						% indices of second block
% 	for ii = 1:nf
% 		for jj = 1:ns
% 			inds = ind:ind+2*nx-1;		% indices of current block
% 			indsr = ind:ind+nx-1;		% indices of real part of current block
% 			indsi = ind+nx:ind+2*nx-1;	% indices of imaginary part of current block
% 			Xi1 = S(inds,1)+G(inds,:)*D*G(1,:)';
% 			diagXi2r = diag(W*S(inds2,indsr))+diagUSVT(W*G(inds2,:),D,G(indsr,:));
% 			diagXi2i = diag(W*S(inds2,indsi))+diagUSVT(W*G(inds2,:),D,G(indsi,:));
% 			b = [b; K(Krs{ii},Kis{ii})*Xi1-omegas(ii)^2*[diagXi2r;diagXi2i] ];
% 			ind = ind+2*nx;
% 		end
% 	end

	b2 = zeros(2*nx,ns,nf);
	inds2 = 2:nw+1;						% indices of second block
	parfor ii = 1:nf
		for jj = 1:ns
			kk = jj+ns*(ii-1);
			inds = 1+nw+((kk-1)*2*nx+1:kk*2*nx);	% indices of current block
			indsr = inds(1:nx);						% indices of real part of current block
			indsi = inds(nx+1:end);					% indices of imaginary part of current block
			Xi1 = S(inds,1)+G(inds,:)*D*G(1,:)';
			diagXi2r = diag(W*S(inds2,indsr))+diagUSVT(W*G(inds2,:),D,G(indsr,:));
			diagXi2i = diag(W*S(inds2,indsi))+diagUSVT(W*G(inds2,:),D,G(indsi,:));
			b2(:,jj,ii) = K(Krs{ii},Kis{ii})*Xi1-omegas(ii)^2*[diagXi2r;diagXi2i];
		end
	end
	b = [S(1,1)+G(1,:)*D*G(1,:)'; reshape(b2,[],1)];
	end

	function X = AT(v)
	% Apply A^T to a vector v. Output a sparse matrix X = A^T(v).
	col = [];
	block_col = [];
	ind = 2;
	for ii = 1:nf
		for jj = 1:ns
			inds = ind:ind+2*nx-1;		% indices of current block
			indsr = ind:ind+nx-1;		% indices of real part of current block
			indsi = ind+nx:ind+2*nx-1;	% indices of imaginary part of current block
			col = [col; KT(Krs{ii},Kis{ii})*v(inds)];
			block_col = [block_col;
						 -omegas(ii)^2*spdiags(v(indsr),0,nx,nx)*W;
						 -omegas(ii)^2*spdiags(v(indsi),0,nx,nx)*W];
			ind = ind+2*nx;
		end
	end
	X = .5*[2*v(1)			sparse(1,nw)	col';
			sparse(nw,1)	sparse(nw,nw)	block_col';
			col				block_col		sparse(ndof,ndof)];
	end

	function w = AATinv(v)
	% Apply (AA^T)^{-1} to a vector v. Output a vector w.
% 	w = v(1);
% 	ind = 2;
% 	for ii = 1:nf
% 		for jj = 1:ns
% 			inds = ind:ind+2*nx-1;		% indices of current block
% 			w = [w; 2*KKTinvs{ii}.apply(v(inds))];
% 			ind = ind+2*nx;
% 		end
% 	end

	w2 = zeros(2*nx,ns,nf);
	parfor ii = 1:nf
		for jj = 1:ns
			kk = jj+ns*(ii-1);
			inds = (kk-1)*2*nx+1:kk*2*nx;	% indices of current block
			w2(:,jj,ii) = 2*KKTinvs{ii}.apply(v(inds));
		end
	end
	w = [v(1); reshape(w2,[],1)];
	end

	function G0 = psd_proj(S,G,D)
	% Project a matrix X of the form X = Xsp + XG*XD*XG' onto the PSD
	% cone, where Xsp is a sparse matrix and XG has few columns, i.e., is
	% a low-rank factor. Return low-rank factor of this projection. MAY NOT
	% BE EXACT!!
	opts.tol = eigs_tol;
	opts.issym = 1;
	if i>1
		opts.v0 = XG;
	end
	Xapp = @(x) S*x+G*(D*(G'*x));
	[vec,lambda] = eigs(Xapp,1+nw+ndof,num_eigvecs,'la',opts);
	fprintf('Number of eigenvectors: %d, number of eigenvalues: %d\n',size(vec,2),size(lambda,1))
	all_lambdas = [all_lambdas diag(lambda)];
	G0 = vec(:,1)*sqrt(lambda(1,1));
	end

	function [S0,G0,D0] = prox_aff_trace(S,G,D)
	% Apply proximal operator of trace(Q*X) + {indicator of affine subspace
	% A(X) = b} to a matrix X that is of the form X = S + G*D*G'.
	% Return a matrix that is sparse plus low rank.
	G0 = G;
	D0 = D;
	S0 = S-Qgam-AT(AATinv(AA(S-Qgam,G0,D0)-B));
	end
end