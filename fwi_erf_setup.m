%% Clear and close everything
% clear all
close all
clc

maxit = 50;

shepp_setup

%% Reconstruction via adjoint state method
fprintf('Spatial dof:%d, inverse prob. dof:%d\n',dom.N,nf*ns*nr)
tic
[m,out]=fwi_erf(dom,freqs,sources,receivers,c_true,c0,sigma,maxit);
toc

%% Post computation reconstrution and objective function
figure 
subplot(1,3,1)
dom.imagesc(c_true);
c_vec=caxis;
hold on
dom.plot(receivers(1,:),receivers(2,:),'rx')
dom.plot(sources(1,:),sources(2,:),'go')
hold off
axis square
title('Problem setup')

subplot(1,3,2)
if sum(m<0)>0, warning('Negative values encountered in m!'), end
dom.imagesc(sqrt(1./m))
caxis(c_vec)
axis square
title('Reconstruction')

subplot(1,3,3)
semilogy(out.J(out.J~=0))
axis square
title('Objective')