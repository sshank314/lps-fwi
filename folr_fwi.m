function folr_fwi(dom,freqs,sources,receivers,ctrue,c0,sigma,maxit)
%FOLR_FWI   First-order lifting/relaxation for full-waveform inversion.

% if isempty(gcp('nocreate')), parpool; end

eigs_tol = 1e-12;
num_eigvecs = 5;
trunc_tol = 1e-12;
Nx = dom.N;								% total number of interior grid points
nf = length(freqs);						% number of frequencies
ns = size(sources,2);					% number of sources
nr = size(receivers,2);					% number of receivers
ndof = 2*Nx*nf*ns;						% total number of degrees of freedom
r_ind = dom.loc2ind(receivers);			% flat indices of receivers
b = zeros(2*Nx,ns);						% initialize right-hand sides
d = zeros(2*nr,nf,ns);					% seismic data
I = speye(2*Nx);						% for padding/prolongation operators
S_pad = I(:,[r_ind r_ind+Nx]);			% padding operator
S_sam = S_pad';
mtrue = 1./ctrue.^2;					% true squared slowness
m0 = 1./c0.^2;							% initial squared slowness
K = @(Kr,Ki) [Kr -Ki;Ki Kr];
KT = @(Kr,Ki) [Kr' Ki';-Ki' Kr'];
diagUSVT = @(U,S,V) sum(U.*(V*S),2);
all_lambdas = [];

% Create discrete deltas at source location
for j = 1:ns
	b(dom.loc2ind(sources(:,j)),j) = 1/(dom.hx*dom.hy);
end
B = [1;repmat(reshape(b,[],1),nf,1)];

omegas = zeros(nf,1);
Krs = cell(nf,1);
Kis = cell(nf,1);
KKTinvs = cell(nf,1);
for i = 1:nf
	[Krs{i},Kis{i},~,omegas(i)] = helmholtz_erf_2d(mtrue,freqs(i),dom);
	KKTinvs{i}=invertA(K(Krs{i},Kis{i})*KT(Krs{i},Kis{i})+omegas(i)^4*I);
end

% Generate true data
fprintf('Generating data... frequency: ')
rng(1)	% set seed
parfor i = 1:nf
	fprintf('%d, ',i)
	[Kr,Ki,M,omega]=helmholtz_erf_2d(mtrue,freqs(i),dom);
	A_true = invertA([Kr -Ki;Ki Kr]-omega^2*[M sparse(Nx,Nx);sparse(Nx,Nx) M]);
	for j = 1:ns
		u_true = A_true.apply(b(:,j)); %#ok<*PFBNS>
		d(:,i,j) = S_sam*u_true+sigma*rand(2*nr,1);
	end
end

Q_col = [];
for i = 1:nf
	for j = 1:ns
		Q_col = [Q_col;-S_pad*d(:,i,j)];
	end
end
Q = [0				sparse(1,Nx)	Q_col';
	 sparse(Nx,1)	sparse(Nx,Nx)	sparse(Nx,ndof)
	 Q_col			sparse(ndof,Nx)	kron(speye(nf*ns),S_pad*S_sam)];
fprintf('Size of Q: %d\n',size(Q,1))

% % Uncomment to do some sanity checks
% U=rand(10,2);
% S=diag(rand(2,1));
% V=rand(10,2);
% USVT=U*S*V';
% disp('Checking diagUSVT...')
% disp(norm(diagUSVT(U,S,V)-diag(USVT))/norm(diag(USVT)))
% 
% b = ones(1+ndof,1);
% ATb = AT(b);
% US = speye(1+Nx+ndof);
% UG = rand(1+Nx+ndof,2);
% % UD = [2 0;0 3];
% % UD = eye(2);
% UD = diag(rand(2,1));
% AU = AA(US,UG,UD);
% ip2 = b'*AU;
% ipfro = trace(US*ATb)+trace(UG'*(ATb*UG*UD));
% disp('Checking adjointness of AA and AT...')
% disp(abs(ip2-ipfro)/abs(ip2))
% 
% b = ones(1+ndof,1);
% w = AATinv(b);
% b1 = AA(AT(w),zeros(1+Nx+ndof,1),0);
% disp('Checking validity of AATinv...')
% disp(norm(b-b1,'fro')/norm(b,'fro'))
% error('Variable names in sanity check clash with variable names of function!')

% Low-rank plus sparse Douglas Rachford iteration
% Initial iterate X0
XG = [1;m0];
for i = 1:nf
	[Kr,Ki,M,omega]=helmholtz_erf_2d(m0,freqs(i),dom);
	A_true = invertA([Kr -Ki;Ki Kr]-omega^2*[M sparse(Nx,Nx);sparse(Nx,Nx) M]);
	for j = 1:ns
		XG = [XG; A_true.apply(b(:,j))];
	end
end
% XG2 = zeros(2*Nx,ns,nf);
% parfor i = 1:nf
% 	[Kr,Ki,M,omega]=helmholtz_erf_2d(m0,freqs(i),dom);
% 	A_true = invertA([Kr -Ki;Ki Kr]-omega^2*[M sparse(Nx,Nx);sparse(Nx,Nx) M]);
% 	for j = 1:ns
% 		XG2(:,j,i) = A_true.apply(b(:,j));
% 	end
% end
% max(abs(XG(Nx+2:end)-reshape(XG2,[],1)))/max(abs(XG(Nx+2:end)))

% Y0 = X0
YG=XG;
YD=1;
YS=sparse(1+Nx+ndof,1+Nx+ndof);

figure
for i=1:maxit
	fprintf('-- ITERATE %d --\n',i)
	
	% Computer prox separately, store in Z
	fprintf('Prox. for affine subspace indicator/trace... ')
	tic
	[ZS,ZG,ZD]=prox_aff_trace(-YS,[XG YG],blkdiag(2*eye(size(XG,2)),-YD));
	t=toc;
	fprintf('%.2f\n',t)
	
	% Y term, using prof above and X, Y
	YS = YS + ZS;
	YG = [XG YG ZG];
	YD = blkdiag(-eye(size(XG,2)),YD,ZD);
	
	% Projection on PSD cone
	fprintf('Projection onto PSD cone... ')
	tic
	XG = psd_proj(YS,YG,YD);
	t=toc; fprintf('%.2f\n',t)
	
	% Truncating Y
	fprintf('Truncating Y...')
	tic
	[YG,YD] = trunc_GDGT(YG,YD,trunc_tol,1);
	t=toc; fprintf('%.2f\n',t)
	
	% Output some information
	disp('Eigenvalues when doing PSD projection...')
	disp(diag(lambda));
	fprintf('NNZ in sparse matrices: Y=%d, Z=%d\n',nnz(YS),nnz(ZS))
	fprintf('Ranks of low-rank factors: X=%d, Y=%d, Z=%d\n',size(XG,2),size(YG,2),size(ZG,2))
	subplot(1,2,1)
		m = XG(2:Nx+1)/sign(XG(2));
		dom.imagesc(m)
		title(sprintf('Iterate %d, min=%.10e, max=%.10e',i,min(m),max(m)))
		colorbar
	subplot(1,2,2)
		semilogy(abs(all_lambdas'))
	drawnow
end

	function b = AA(S,G,D)
	% Applies A to a matrix of the form X = S + G*D*G', where S is sparse,
	% G has few columns, and D is diagonal. Outputs a vector b = A(X).
	b = S(1,1)+G(1,:)*D*G(1,:)';		% initialize b = X_11
	ind = Nx+2;							% starting index
	inds2 = 2:Nx+1;						% indices of second block
	for ii = 1:nf
		for jj = 1:ns
			inds = ind:ind+2*Nx-1;		% indices of current block
			indsr = ind:ind+Nx-1;		% indices of real part of current block
			indsi = ind+Nx:ind+2*Nx-1;	% indices of imaginary part of current block
			Xi1 = S(inds,1)+G(inds,:)*D*G(1,:)';
			% Note that the two lines below use diag(U*V') = sum(U.*V,2)
			diagXi2r = diag(S(indsr,inds2))+diagUSVT(G(indsr,:),D,G(inds2,:));
			diagXi2i = diag(S(indsi,inds2))+diagUSVT(G(indsi,:),D,G(inds2,:));
			b = [b; K(Krs{ii},Kis{ii})*Xi1-omegas(ii)^2*[diagXi2r;diagXi2i] ];
			ind = ind+2*Nx;
		end
	end
	end

	function X = AT(v)
	% Apply A^T to a vector v. Output a sparse matrix X = A^T(v).
	col = [];
	block_col = [];
	ind = 2;
	for ii = 1:nf
		for jj = 1:ns
			inds = ind:ind+2*Nx-1;		% indices of current block
			indsr = ind:ind+Nx-1;		% indices of real part of current block
			indsi = ind+Nx:ind+2*Nx-1;	% indices of imaginary part of current block
			col = [col; KT(Krs{ii},Kis{ii})*v(inds)];
			block_col = [block_col;
						 -omegas(ii)^2*spdiags(v(indsr),0,Nx,Nx);
						 -omegas(ii)^2*spdiags(v(indsi),0,Nx,Nx)];
			ind = ind+2*Nx;
		end
	end
	X = .5*[2*v(1)			sparse(1,Nx)	col';
			sparse(Nx,1)	sparse(Nx,Nx)	block_col';
			col				block_col		sparse(ndof,ndof)];
	end

	function w = AATinv(v)
	% Apply (AA^T)^{-1} to a vector v. Output a vector w.
	w = v(1);
	ind = 2;
	for ii = 1:nf
		for jj = 1:ns
			inds = ind:ind+2*Nx-1;		% indices of current block
			w = [w; 2*KKTinvs{ii}.apply(v(inds))];
			ind = ind+2*Nx;
		end
	end
	end

	function G0 = psd_proj(S,G,D)
	% Project a matrix X of the form X = Xsp + XG*XD*XG' onto the PSD
	% cone, where Xsp is a sparse matrix and XG has few columns, i.e., is
	% a low-rank factor. Return low-rank factor of this projection. MAY NOT
	% BE EXACT!!
	opts.tol = eigs_tol;
	opts.issym = 1;
	if i>1
		opts.v0 = XG;
	end
	Xapp = @(x) S*x+G*(D*(G'*x));
	[vec,lambda] = eigs(Xapp,1+Nx+ndof,num_eigvecs,'la',opts);
	fprintf('Number of eigenvectors: %d, number of eigenvalues: %d\n',size(vec,2),size(lambda,1))
	all_lambdas = [all_lambdas diag(lambda)];
	G0 = vec(:,1)*sqrt(lambda(1,1));
	end

	function [S0,G0,D0] = prox_aff_trace(S,G,D)
	% Apply proximal operator of trace(Q*X) + {indicator of affine subspace
	% A(X) = b} to a matrix X that is of the form X = S + G*D*G'.
	% Return a matrix that is sparse plus low rank.
	G0 = G;
	D0 = D;
	S0 = S-Q-AT(AATinv(AA(S-Q,G0,D0)-B));
	end
end