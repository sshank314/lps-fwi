%% -- SETUP FILE FOR PROXIMAL SPLITTING METHODS/LIFTING OF FULL-WAVEFORM INVERSION -- %%
% Clear and close everything
clear all
close all
clc

maxit = 100000;
method = 'dr';
padding = 'transpose';
init = 'smoothed_true';
ctr = 2;
square_setup

%% Run first-order lifting/relaxation for FWI
fprintf('Spatial dof:%d, inverse prob. dof:%d\n',dom.N,nf*ns*nr)
tic
dm=pocs_fwi_window(dom,all_freqs,sources,receivers,c_true,c_0,window_info,sigma,maxit,...
	'method',method,'padding',padding,'eigs_tols',1e-8);
fprintf('Elapsed time: %.3f\n',toc)