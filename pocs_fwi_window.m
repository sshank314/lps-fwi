function m=pocs_fwi_window(dom,all_freqs,sources,receivers,c_true,c_0,window_info,sigma,maxit,varargin)

% Default variables for varargin
k_plot = 100;			% specifies plotting frequency
eigs_tol = 1e-4;		% tolerance passed to eigs
method='pocs';			% proximal splitting method to use
num_eigvecs = 2;		% number of eigenvectors calculated by eigs
padding = 'transpose';	% 

% Process varargin
for k = 1:2:length(varargin)
	switch varargin{k}
		case 'k_plot'
			k_plot = varargin{k+1};
		case 'eigs_tols'
			eigs_tol = varargin{k+1};
		case 'method'
			method = varargin{k+1};
			if strcmp(method,'dr')
				trunc_tol = 1e-8; %#ok<*NASGU>
			end
		case 'num_eigvecs'
			num_eigvecs = varargin{k+1};
		case 'padding'
			padding = varargin{k+1};
	end
end

Nx = dom.N;									% total number of grid points
[win_inds,~,W] = dom.window(window_info);	% quantities for windowing
nf = length(all_freqs);						% number of frequencies
nw = length(win_inds);						% number of pixels in window
ns = size(sources,2);						% number of sources
nr = size(receivers,2);						% number of receivers
fields_dof = 2*Nx*nf*ns;					% total number of degrees of freedom
r_ind = dom.loc2ind(receivers);				% flat indices of receivers
I = speye(2*Nx);							% for padding/prolongation operators
S_sam = I([r_ind r_ind+Nx],:);				% sampling operator
if strcmp(padding,'transpose')				% padding operator
	S_pad = S_sam';
else
	S_pad = S_sam'/(dom.hx*dom.hy);
end
m_true = 1./c_true.^2;						% true squared slowness
m_0 = m_true;								% initial squared slowness
m_0(win_inds) = 1./c_0(win_inds).^2;		% set value outside window to initial value
mm_size = 1+nw+fields_dof;					% size of the full moment matrix
ctr = max(c_true)/min(c_true);				% contrast
xopts.tol = eigs_tol;
xopts.num = num_eigvecs;
yopts = xopts;
b = eqrf(generate_sources(dom,sources));
d = eqrf(generate_seismic_data(dom,sources,receivers,all_freqs,m_true,sigma));
fprintf('Total number of unknowns: %d\n',mm_size)

% Factor matrices required for affine projection
K = @(Kr,Ki) [Kr -Ki;Ki Kr];
KT = @(Kr,Ki) [Kr' Ki';-Ki' Kr'];

omegas = zeros(nf,1);
Krs = cell(nf,1);
Kis = cell(nf,1);
KKTinvs = cell(nf,1);
for i = 1:nf
	[Krs{i},Kis{i},~,omegas(i)] = helmholtz_erf_2d(m_true,all_freqs(i),dom);
	Krs{i} = Krs{i} - omegas(i)^2*spdiags(m_0,0,Nx,Nx);
	KKTinvs{i}=invertA( ...
		[K(Krs{i},Kis{i})*KT(Krs{i},Kis{i})+omegas(i)^4*kron(speye(2),W*W')	K(Krs{i},Kis{i})*S_pad
		S_sam*KT(Krs{i},Kis{i})												S_sam*S_pad] );
end

% UNCOMMENT TO DO SOME SANITY CHECKS
% Check adjointness
ell = 1+fields_dof+2*nr*nf*ns;
XStest = spdiags((1:mm_size)',0,mm_size,mm_size);
XGtest = rand(mm_size,2);
XDtest = diag([2 3]);
Xtest = ssplr(XStest,XGtest,XDtest);
ytest = rand(ell,1);
ATytest = ssplr(AT(ytest),zeros(mm_size,1),0);
frob_ip = dot(ATytest,Xtest);	% <A^T(y),S+G*D*G^T>_F
two_ip = dot(ytest,A(Xtest));
fprintf('Relative error in <A^T(y),X>_F vs <y,A(X)>_2: %.3e\n', ...
	abs(frob_ip-two_ip)/abs(two_ip) )

% Check AATinv...
fprintf('Relative error in (AA^T)^(-1): %.3e\n', ...
	norm(ytest - AATinv(A(ATytest)),'inf')/norm(ytest,'inf') )

% Set up initial iterate and rhs
X = ssplr(sparse(mm_size,mm_size),rank1col(zeros(nw,1)),1);
Y = ssplr(sparse(mm_size,mm_size),zeros(mm_size,1),0);
B = [1;repmat(reshape(b,[],1),nf,1)];
for i = 1:nf
	for j = 1:ns
		B = [B; d(:,i,j)];
	end
end
normB = norm(B);

prox_aff = @(X) X+ssplr(-AT(AATinv(A(X)-B)),zeros(mm_size,1),0);
evs=[];
misuvec = [];
mismvec = [];
errvec = [];
Y_feas = [];
X_feas = [];
X_11 = [];
all_lambdas = [];
max_c = [];
min_c = [];
fig = figure;

for k = 1:maxit
	% Projection onto convex sets
	if strcmp(method,'pocs')
		[X,Y] = pocs_step(X);
	else
		[X,Y] = dr_step(X,Y);
		Y = rank1_proj(Y,yopts);
		yopts.v0 = Y.G;
	end
	
	% Reuse information for next iterate
	if k > 1, xopts.v0 = X.G; end
	
	dm = X(2:nw+1,1)/sign(X(1,1));
	m = m_0+W*dm;
	
	% Grab relevant information and plot
	if mod(k,k_plot)==0
		x_axis = k_plot:k_plot:k;
		
		% Calculate feasibility of each iterate
		Y_feas(end+1) = norm(A(Y)-B)/normB;
		X_feas(end+1) = norm(A(X)-B)/normB;
		X_11(end+1) = X(1,1);

		% Extract perturbation, calculate relevant quantities
		misuvec(end+1) = misfit_u(X);
		mismvec(end+1) = misfit_dm(dm);
		m = m_0+W*dm;
		c = sqrt(1./(m));
		errvec(end+1) = dom.error(c,c_true);
		max_c(end+1) = max(c);
		min_c(end+1) = min(c);
		all_lambdas(end+1,:) = evs;
		
		figure(fig)
		subplot(2,3,1)
			dom.imagesc(c)
			title(sprintf('Iterate %d, min=%.2e, max=%.2e',k,min(c),max(c)))
			caxis([1 ctr])
		subplot(2,3,2)
			semilogy(x_axis,misuvec,'-r',x_axis,mismvec,'-b')
			title('Misfit: u (red), dm (blue)')
		subplot(2,3,3)
			semilogy(x_axis,errvec)
			title('Error')
		subplot(2,3,4)
			semilogy(x_axis,abs(all_lambdas))
			title('Eigenvalues of affine projection')
		subplot(2,3,5)
			semilogy(x_axis,X_feas)
			title('Feasibility of X')
		subplot(2,3,6)
			plot(x_axis,X_11,'-r',x_axis,max_c,'-b',x_axis,min_c,'-k')
			title('X_{11} (red), max(c) (blue), min(c) (black)')
		drawnow
	end
end

	function y=A(X)
	S = X.S;
	G = X.G;
	D = X.D;
	diagUSVT = @(U,S,V) sum(U.*(V*S),2);
	y2 = [];
	y3 = [];
	inds2 = 2:nw+1;
	for ii = 1:nf
		for jj = 1:ns
			kk = jj+ns*(ii-1);
			inds = 1+nw+(1+2*(kk-1)*Nx:2*kk*Nx);
			indsr = inds(1:Nx);
			indsi = inds(Nx+1:end);
			Xi1 = S(inds,1)+G(inds,:)*D*G(1,:)';
			diagXi2r = diag(S(indsr,inds2)*W')+diagUSVT(G(indsr,:),D,W*G(inds2,:));
			diagXi2i = diag(S(indsi,inds2)*W')+diagUSVT(G(indsi,:),D,W*G(inds2,:));
			y2 = [y2; K(Krs{ii},Kis{ii})*Xi1 - omegas(ii)^2*[diagXi2r;diagXi2i] ];
			y3 = [y3 ;S_sam*Xi1];
		end
	end
	y = [S(1,1)+G(1,:)*D*G(1,:)'; y2; y3];
	end

	function S=AT(y)
	col = [];
	block_col = [];
	for ii = 1:nf
		for jj = 1:ns
			kk = jj+ns*(ii-1);
			inds2 = 1+(1+2*(kk-1)*Nx:2*kk*Nx);
			inds2r = inds2(1:Nx);
			inds2i = inds2(Nx+1:end);
			inds3 = 1+fields_dof+(1+2*(kk-1)*nr:2*kk*nr);
			KK = K(Krs{ii},Kis{ii});
			col = [col; KK'*y(inds2)+S_pad*y(inds3)];
			block_col = [block_col;
				-omegas(ii)^2*[spdiags(y(inds2r),0,Nx,Nx);
							   spdiags(y(inds2i),0,Nx,Nx)]*W];
		end
	end
	S = .5*[2*y(1)			sparse(1,nw)	col';
			sparse(nw,1)	sparse(nw,nw)	block_col';
			col				block_col		sparse(fields_dof,fields_dof)];
	end

	function y=AATinv(x)
	y2=[];
	y3=[];
	for ii = 1:nf
		for jj = 1:ns
			kk = jj+ns*(ii-1);
			inds2 = 1+(1+2*(kk-1)*Nx:2*kk*Nx);
			inds3 = 1+fields_dof+(1+2*(kk-1)*nr:2*kk*nr);
			y_temp = KKTinvs{ii}.apply([x(inds2);x(inds3)]);
			y2 = [y2;y_temp(1:2*Nx)];
			y3 = [y3;y_temp(2*Nx+1:end)];
		end
	end
	y = [x(1);2*y2;2*y3];
	end

	function J = misfit_dm(dm)
	J = 0;
	for ii = 1:nf
		[Krm,Kim,Mm,omegam] = helmholtz_erf_2d(m_0+W*dm,all_freqs(ii),dom);
		Ainv = invertA(Krm+1i*Kim-omegam^2*Mm);
		for jj = 1:ns
			um = Ainv.apply(b(1:Nx,jj));
			J = J + norm(S_sam*[real(um);imag(um)]-d(:,ii,jj))^2;
		end
	end
	end

	function J = misfit_u(X)
	J = 0;
	for ii = 1:nf
		for jj = 1:ns
			kk = jj+ns*(ii-1);
			inds = 1+nw+(1+2*(kk-1)*Nx:2*kk*Nx);
			Xi1 = X(inds,1);
			J = J + norm(S_sam*Xi1-d(:,ii,jj))^2;
		end
	end
	end

	function x0 = rank1col(dm)
	x0 = [1; zeros(nw,1)];
	for ii = 1:nf
		[Kr,Ki,M,omega]=helmholtz_erf_2d(m_0+W*dm,all_freqs(ii),dom);
		A_inv = invertA([Kr -Ki;Ki Kr]-omega^2*[M sparse(Nx,Nx);sparse(Nx,Nx) M]);
		for jj = 1:ns
			x0 = [x0; A_inv.apply(b(:,jj))];
		end
	end
	end

	function [Xnew,Ynew] = pocs_step(Xold) %#ok<*DEFNU>
	Ynew = prox_aff(Xold);
	[Xnew,evs] = rank1_proj(Ynew,xopts);
	end

	function [Xnew,Ynew] = dr_step(Xold,Yold)
	Ynew = prox_aff(2*Xold-Yold)-Xold+Yold;
	[Xnew,evs] = rank1_proj(Ynew,xopts);
	end
end