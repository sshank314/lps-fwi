%% -- SETUP FILE FOR FIRST-ORDER LIFTING/RELAXATION OF FULL-WAVEFORM INVERSION -- %%
%% Clear and close everything
clear all
close all
clc

maxit=2500;
window_info.type='all';

shepp_setup

%% Run first-order lifting/relaxation for FWI
fprintf('Spatial dof:%d, inverse prob. dof:%d\n',dom.N,nf*ns*nr)
folr_fwi(dom,freqs,sources,receivers,c_true,c0,sigma,maxit);