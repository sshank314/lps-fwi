%% Problem setup and parameters
fmin = 4.5;							% minimum frequency
fmax = 6;							% maximum frequency
nf = 4;								% number of frequencies
all_freqs = linspace(fmin,fmax,nf);	% frequencies
ns = 4;								% number of sources
nr = 11;							% number of receivers
sigma = 0;							% noise level
smoothness = .1;					% intensity of smoothing filter
window_info.type = 'rectangle_inner';
window_info.bounds = [.25 .7 .3 .75];

%% Square
nx = 18;							% number of gridpoints in a given direction
dom = domain([0 1 0 1],[nx nx]);	% square domain with equispaced gridpoints
square_bounds = [.4 .6 .4 .6];
c_true = dom.mat2vec(squaremodel(dom,ctr,square_bounds,smoothness));
switch init
	case 'constant'
		c_0 = dom.mat2vec(ones(nx));
	case 'smoothed_true'
		c_0 = dom.mat2vec(smooth(dom.vec2mat(c_true),2*smoothness));
	case 'true'
		c_0 = c_true;
end

sources = [.2 .8 .5 .5;
	       .6 .6 .2 .8];
receivers = [.2 .4 .65 .75 .2 .4 .65 .75 .2 .75;
		     .2 .2 .2  .2  .8 .8 .8  .8  .4 .4];

[~,W] = dom.window(window_info);
wpml = .15;
pml_info.type = 'pml';
pml_info.width = wpml;
[~,PML] = dom.window(pml_info);

%% Preliminary plots of experimental setup
figure

subplot(2,2,1)
dom.imagesc(c_true);
c_vec = caxis;
hold on
dom.plot(receivers(1,:),receivers(2,:),'rx')
dom.plot(sources(1,:),sources(2,:),'go')
hold off
axis square
title(sprintf('Problem setup, contrast:%.2f',max(c_true)/min(c_true)))

subplot(2,2,2)
dom.imagesc(c_0)
axis square
title('Initial background velocity')
caxis(c_vec)

subplot(2,2,3)
imagesc(dom.vec2mat(c_true)+PML)
hold on
dom.plot(receivers(1,:),receivers(2,:),'rx')
dom.plot(sources(1,:),sources(2,:),'go')
hold off
title('With PML')
axis square
caxis([c_vec(1) c_vec(2)])

subplot(2,2,4)
imagesc(dom.vec2mat(c_true)+flipud(W))
hold on
dom.plot(receivers(1,:),receivers(2,:),'rx')
dom.plot(sources(1,:),sources(2,:),'go')
hold off
title('With window')
axis square
caxis([c_vec(1) c_vec(2)+1])

drawnow