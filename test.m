% Tests for ssplr
clc
clear all
close all

% Setup
n = 10;
rX = 2;
rY = 3;
rng(1)
SX = speye(n);
GX = rand(n,rX);
DX = diag([1 2]);
SY = spdiags((1:n)',0,n,n);
GY = rand(n,rY);
DY = diag([3 4 5]);
X = ssplr(SX,GX,DX);
Y = ssplr(SY,GY,DY);

% Sum
Z = X+Y;
fprintf('Difference between sums: %.2e\n',norm(full(Z)-(full(X)+full(Y)))/norm(full(Z)))

% Scalar multiple
alpha = 2;
Z = alpha*X;
fprintf('Difference between scalar multiples: %.2e\n',norm(full(Z)-alpha*full(X))/norm(full(Z)))

% Inner product
alpha = trace(full(X)*full(Y));
fprintf('Difference between inner products: %.2e\n',abs(alpha-dot(X,Y))/abs(alpha))

% Norm
fprintf('Difference between norms: %.2e\n',abs(norm(X)-norm(full(X),'fro'))/norm(full(X),'fro'))

% Subtraction
fprintf('Difference between negation: %.2e\n',norm(full(-X)-(-full(X)),'fro')/norm(full(X),'fro'))

% Subscripting
Xfull = full(X);
l_inds = [2 3 4];
r_inds = [5 6 7];
fprintf('Difference between subscripts: %.2e\n',norm(Xfull(l_inds,r_inds)-X(l_inds,r_inds)))